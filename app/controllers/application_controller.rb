class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  #All helpers available in views by default, not in controllers
  include SessionsHelper #For sign-in and 'remember me section'
end
