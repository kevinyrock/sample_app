module SessionsHelper

	# Generates a remember token
	# Stores token in a cookie on user's machine
	# Also stores encrypted version of the key on server
	# This key will be changed/updated with each successful
	# 	user sign-in
	def sign_in(user)
		remember_token = User.new_remember_token
		cookies.permanent[:remember_token] = remember_token
		user.update_attribute(:remember_token, User.encrypt(remember_token))
		self.current_user = user
	end

	def signed_in?
		#Establishes user state to change links in the layout.
		!current_user.nil?
	end

	def current_user=(user)
		@current_user = user
	end

	def current_user
		# Retrieven remember token from cookie and encrypt
		# Use encrypted token to search database
		# Using ||= operator hits database only once with subsequent
		# 	uses of current_user
		remember_token = User.encrypt(cookies[:remember_token])
		@current_user ||= User.find_by(remember_token: remember_token)
		## ||= Explanation ##
		# Follows 'short-circuit evaluation' 
		# @user
		# => nil
		# @ user = @user || "the user"
		# => "the user"
		# @user = @user || "another user"
		# => "the user"
	end

	def current_user?(user)
		user == current_user
	end

	def signed_in_user
		unless signed_in?
			store_location
			redirect_to signin_url, notice: "Please sign in."
		end
	end

	def sign_out
		self.current_user = nil
		cookies.delete(:remember_token)
	end

	# Code to redirect user if:
	# they wanted edit page, they aren't logged in
	# log in and direct to edit page instead of root

	def redirect_back_or(default)
		redirect_to(session[:return_to] || default)
		session.delete(:return_to)
	end

	def store_location
		session[:return_to] = request.url if request.get?
	end

	#/code to redirect user
end
